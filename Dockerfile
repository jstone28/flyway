FROM flyway/flyway:6.2.3

COPY run-flyway /run-flyway

ENTRYPOINT ["/run-flyway"]
